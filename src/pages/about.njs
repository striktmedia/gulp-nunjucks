---
message: Bert
layout: default
outputFile: over
---

{% extends path.templates + layout + ext %}

{% block pagetitle %}{{ pagetitle }}{% endblock %}

{% block body %}

{% include path.partials + "nav" + ext %}

  <h1>{{ pagetitle }}</h1>
  yaml: Hello {{ message }}

{% endblock %}
