hier komt de navigatie:

<ul>
  {%- for item in nav %}
    <li>{{ item.title }}: {{ item.link }}</li>
  {%- endfor %}
</ul>
