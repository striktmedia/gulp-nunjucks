<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width">
    <title>{% block pagetitle %}{% endblock %}</title>
    <link rel="alternate" href="/feed.xml" type="application/rss+xml">
    <link rel="stylesheet"
          href="http://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic|Anonymous+Pro:400,700,400italic,700italic|Merriweather:400,700,300">
    <link rel="stylesheet" href="/assets/css/main.css">
</head>
<body>
<header class="header">
    <div class="content-wrap">
        <div class="logo"><h1><a></a></h1>

            <p class="description"></p></div>
    </div>
</header>
<div id="content">
    {% block body %}
        <div class="content-wrap"><h2>Welcome to zombocom!</h2></div>
    {% endblock %}
</div>
<footer>
    <div class="content-wrap">
        <section class="about"></section>
        <section class="copy"><p>&copy; 2015 &mdash; powered by&nbsp;<a href="https://github.com/jnordberg/wintersmith">Wintersmith</a>
        </p></section>
    </div>
</footer>
</body>
</html>
