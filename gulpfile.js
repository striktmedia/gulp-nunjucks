var gulp = require('gulp'),
    nunjucksRender = require('gulp-nunjucks-api'),
    data = require('gulp-data'),
    del = require('del'),
    fm = require('front-matter'),
    rename = require('gulp-rename'),
    gutil = require('gulp-util'),
    log = require('gulp-util').log,
    path = require('path'),
    cfg = require('./src/data/config.json');


function getYamlDataForFile(file){
  var content = fm(String(file.contents));
  // file.contents = new Buffer(content.body);
  // console.log(content);
  // gutil.log( gutil.colors.red( content.attributes ));
  return content.attributes;
}

gulp.task("clean", function(cb) {
  del(cfg.path.dist, cb);
});

gulp.task("build", ["clean"], function () {
  return gulp.src(cfg.path.pages + '/*.njs')
    .pipe(data(getYamlDataForFile))
    .pipe(nunjucksRender({
      src: cfg.path.source,
      extension: ".njs",
      data: require('./'+ cfg.path.data +'/global.json'),
      locals: "<filename_noext>.json",
      verbose: true
    }))
    .pipe(data(function(file) {
      log( gutil.colors.red( gutil.replaceExtension( path.basename(file.path), '') ));
      var jsonData = require('./'+ cfg.path.pages +'/' + gutil.replaceExtension( path.basename(file.path), '') + '.json');
      var outputFile = jsonData.outputFile;
      gutil.log( gutil.colors.blue(outputFile) );
      // return jsonData;
    }))
    // .on('end', function() {
    //   gutil.log( gutil.colors.blue(outputFile) );
    // })
    .pipe(rename({
      extname: ".html"
    }))
    .pipe(gulp.dest('dist'));
});

gulp.task("watch", function() {
  gulp.watch([
    cfg.path.pages + '/*',
    cfg.path.data + '/*',
    cfg.path.templates + '/*',
    cfg.path.partials + '/*'
  ], ["build"]);
});


gulp.task("default", ["build"]);
